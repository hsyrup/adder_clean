#include "Adder.hpp"
#include <climits>

Adder::Adder(int k){
  //cout << "begin adder" << endl;
  //A0,A1,...B0,B1,...,C0
  for(int i = 0; i < 2*k+1; i++) {
    queue<Event*> *q = new queue<Event*>;
    queue_collection1.push_back(q);
    if(i >= 0 && i<k) queue1_name.push_back("A_"+to_string(i));
    else if(i >= k && i < 2*k) queue1_name.push_back("B_"+to_string(i%k));
    else queue1_name.push_back("C_0");
  }
  //C1,C2,...C8
  for(int i = 0; i < k; i++) {
    queue<Event*> *q = new queue<Event*>;
    queue_collection2.push_back(q);
    int index = i+1;
    queue2_name.push_back("C_"+to_string(index));
  }
  //S0,S1,...,S7
  for(int i = 0; i < k; i++) {
    queue<Event*> *q = new queue<Event*>;
    queue_collection3.push_back(q);
    queue3_name.push_back("S_"+to_string(i));
  }

  for(int i = 0; i < k; i++) {
    if(i == 0) {
      Graph *a = new Graph(i, queue_collection1[i], queue_collection1[i+k], queue_collection1[i+2*k], queue_collection2[i], queue_collection3[i]);
      graph_collection.push_back(a);
    } else {

      Graph *a = new Graph(i, queue_collection1[i], queue_collection1[i+k], queue_collection2[i-1], queue_collection2[i], queue_collection3[i]);
      graph_collection.push_back(a);
    }
    graph_name.push_back("graph_"+to_string(i));
    //cout << a << endl;
  }

  //cout << "end adder" << endl;
  // for(int i = 0; i < k; i++) {
  //   cout << graph_collection[i].graphID << endl;
  // }

  // for(int i = 0; i < 2*k; i++) {
  //   cout <<"Queue Name"<< queue1_name[i] << "Value" << queue_collection1[i].front() << endl;
  // }

}

void Adder::finalOutput(){
  int k = queue_collection3.size();
  //cout << "start final output" << k << endl;
  vector<int> tmp(k, -1);

  std::ofstream myfile;
  myfile.open("output.txt");

  int ctr = 0;
  long clock = LONG_MAX;
  long sum = 0;
  //while(ctr < k){
  for(int n = 0; n < 100; n++){
    //terminate cond
    // for(int i = 0; i < k; i++){
    //   cout << "size of collec[" <<i<<"]:"<<queue_collection3[i]->size()<<endl;
    // }

    //update clock && term cod
    ctr = 0;
    clock = 99999;
    for(int i = 0; i < k; i++){
      if(queue_collection3[i]->empty()){
        //cout << "time of collec[" <<i<<"]:" << "empty"<< endl;
        ctr++;
      } else if(queue_collection3[i]->front()->recvTime < clock){
        clock = queue_collection3[i]->front()->recvTime;
        //cout << "time of collec[" <<i<<"]:" << queue_collection3[i]->front()->recvTime <<",clock:"<<clock<< endl;
      }else {
        //cout << "time of collec[" <<i<<"]:" << queue_collection3[i]->front()->recvTime <<",clock:"<<clock<< endl;
      }
    }
    if(ctr >= k) break;

    //populate && exec events
    for(int i = 0; i < k; i++){
      if(!queue_collection3[i]->empty() && queue_collection3[i]->front()->recvTime <= clock){
        tmp[i] = queue_collection3[i]->front()->val;
        queue_collection3[i]->pop();
        //cout << "pop succeed, clec[" <<i<<"]:"<< queue_collection3[i]->size() << endl;
      }
    }

    // cout<<"output run:" << endl;
    // for(int i = 0; i < k; i++){
    //   cout << "tmp[" << i << "]:" << tmp[i] <<",";
    // }
    // cout << endl;
    //compute sum
    sum = 0;
    for(int i = k-1; i >= 0; i--){
      if(tmp[i] != -1){
        sum = 2*sum + tmp[i];
      }
      else {
        sum = 2*sum;
      }
    }
    myfile << sum << endl;
    clock++;
  }
  //cout << "final output finished with sum:" << sum <<endl;
  myfile.close();
}

void Adder::Read(int a_input, int b_input, long receive_time){
  int k = 8;
  // string a_binary = bitset<8>(a_input).to_string();
  // string b_binary = bitset<8>(b_input).to_string();
  // string a_binary = "00001111";
  // string b_binary = "00001010";
  // std::vector<int> ain {1,1,1,1,1,1,1,1};
  // std::vector<int> bin {1,1,1,1,1,1,1,1};
  int ain[8];
  int bin[8];
  bitset<8> input_1 = a_input;
  bitset<8> input_2 = b_input;
  for(int i=0,j=7;i<8;i++,j--)
  {
     ain[i] = input_1[j];
     bin[i] = input_2[j];
  }
  int count = 0;
  // Event a0("REGULAR",ain[0]);
  // a0.recvTime = receive_time;
  // queue_collection1[count++]->push(&a0);
  // Event a1("REGULAR",ain[1]);
  // a1.recvTime = receive_time;
  // queue_collection1[count++]->push(&a1);
  // Event a2("REGULAR",ain[2]);
  // a2.recvTime = receive_time;
  // queue_collection1[count++]->push(&a2);
  // Event a3("REGULAR",ain[3]);
  // a3.recvTime = receive_time;
  // queue_collection1[count++]->push(&a3);
  // Event a4("REGULAR",ain[4]);
  // a4.recvTime = receive_time;
  // queue_collection1[count++]->push(&a4);
  // Event a5("REGULAR",ain[5]);
  // a5.recvTime = receive_time;
  // queue_collection1[count++]->push(&a5);
  // Event a6("REGULAR",ain[6]);
  // a6.recvTime = receive_time;
  // queue_collection1[count++]->push(&a6);
  // Event a7("REGULAR",ain[7]);
  // a7.recvTime = receive_time;
  // queue_collection1[count++]->push(&a7);
  //
  // Event b0("REGULAR",bin[0]);
  // b0.recvTime = receive_time;
  // queue_collection1[count++]->push(&b0);
  // Event b1("REGULAR",bin[1]);
  // b1.recvTime = receive_time;
  // queue_collection1[count++]->push(&b1);
  // Event b2("REGULAR",bin[2]);
  // b2.recvTime = receive_time;
  // queue_collection1[count++]->push(&b2);
  // Event b3("REGULAR",bin[3]);
  // b3.recvTime = receive_time;
  // queue_collection1[count++]->push(&b3);
  // Event b4("REGULAR",bin[4]);
  // b4.recvTime = receive_time;
  // queue_collection1[count++]->push(&b4);
  // Event b5("REGULAR",bin[5]);
  // b5.recvTime = receive_time;
  // queue_collection1[count++]->push(&b5);
  // Event b6("REGULAR",bin[6]);
  // b6.recvTime = receive_time;
  // queue_collection1[count++]->push(&b6);
  // Event b7("REGULAR",bin[7]);
  // b7.recvTime = receive_time;
  // queue_collection1[count++]->push(&b7);
  for(int i = k-1; i >= 0; i--) {
    Event *a = new Event("REGULAR",ain[i]);
    a->recvTime = receive_time;
    queue_collection1[count]->push(a);
    // Event *n = new Event("NULL",-1);
    // n->recvTime = receive_time+1;
    // queue_collection1[count]->push(n);
    count++;
  }
  for(int i = k-1; i >= 0; i--) {
    Event *b = new Event("REGULAR",bin[i]);
    b->recvTime = receive_time;
    queue_collection1[count]->push(b);
    // Event *n = new Event("NULL",-1);
    // n->recvTime = receive_time+1;
    // queue_collection1[count]->push(n);
    count++;
  }

  Event *c = new Event("REGULAR",0);
  c->recvTime = receive_time;
  queue_collection1[count]->push(c);
  // Event *n = new Event("NULL",-1);
  // n->recvTime = receive_time+1;
  // queue_collection1[count]->push(n);
  //cout << "count " << count << endl;

  // for(int i = 0; i < 2*k+1; i++) {
  //   cout <<"Queue Name"<< queue1_name[i] << "Value" << (queue_collection1[i]->front())->val << " size " << queue_collection1[i]->size()<< endl;
  // }

  //cout << "aaa" <<endl;
}
