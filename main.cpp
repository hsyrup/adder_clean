//
//  main.cpp
//  6730proj2
//
//  Created by cheng on 18/3/26.
//  Copyright © 2018年 cheng. All rights reserved.
//

//Class: GNode == SimObj
//       Graph
//
#include <iostream>
#include "Graph.hpp"
#include "GNode.hpp"
#include "SimObj.hpp"
#include "Event.hpp"
#include "Adder.hpp"
#include <queue>
#include <bitset>
#include <sys/time.h>
#include <omp.h>

using namespace std;

static double get_walltime(void)
{
  struct timeval tp;
  gettimeofday(&tp, NULL);

  return (double) (tp.tv_sec + tp.tv_usec/1.e6);
}



int main(int argc, const char * argv[]) {

    int k = 8;
    int epi = 10;
    double walltime;

    Adder a(k);

    walltime = 0;
    for(int testtime = 0; testtime < 200; testtime++){
      //cout << "queue front event" << (a.queue_collection1[0])->size() << endl;
      a.Read(31,37,(long)(testtime + 3));
      //cout << "queue front event A1" << (*(a.queue_collection1[1]->front())).val << endl;
      walltime -= get_walltime();
      //#pragma omp parallel for
      for(int i = 0; i < k; i++){
        //cout << "before receive" << endl;
        //cout << "Adder Name: " << a.graph_name[i]<< endl;
        //cout << "queue front eventA" << i <<" "<< (*(a.queue_collection1[i]->front())).val << endl;
        a.graph_collection[i]->Receive();
        //cout << "receive at testtime " << testtime << " Adder No. " << i << endl;
        a.graph_collection[i]->Excute(epi);
        //a.graph_collection[i]->Excute(epi);
        //cout << "excute at testtime " << testtime << " Adder No. " << i << endl;
      }
      walltime += get_walltime();
      a.finalOutput();
    }

    cout <<"time cost: " << walltime << endl;

    return 0;

    //
    // queue<Event*> inputA;
    // queue<Event*> inputB;
    // queue<Event*> inputC;
    // queue<Event*> outputC;
    // queue<Event*> outputS;
    //
    // Graph g(1, &inputA, &inputB, &inputC, &outputC, &outputS);
    //
    // int epi = 10;
    //
    // //Events
    // Event e1("REGULAR", 1);
    // Event e2("REGULAR", 1);
    // Event e3("REGULAR", 1);
    // Event e6("NULL", -1);
    // Event e7("REGULAR", 1);
    // Event e8("REGULAR", 0);
    // Event e9("REGULAR", 0);
    // e1.recvTime = 0l;
    // e2.recvTime = 0l;
    // e3.recvTime = 0l;
    // e7.recvTime = 1l;
    // e8.recvTime = 1l;
    // e9.recvTime = 1l;
    // e6.recvTime = 2l;
    //
    // inputA.push(&e1);
    // inputB.push(&e2);
    // inputC.push(&e3);
    //
    // inputA.push(&e7);
    // inputB.push(&e8);
    // inputC.push(&e9);
    //
    // inputA.push(&e6);
    // inputB.push(&e6);
    // inputC.push(&e6);
    //
    // g.Receive();
    //
    // // //check
    // // cout << "initial events" <<endl;
    // // for(int i = 0; i < 3; i++){
    // //     cout << "nodes[" << i <<"]:";
    // //     cout << g.nodes[i]->data.inputEvents[0].front().val << endl;
    // // }
    //
    // g.Excute(epi);
    //
    // for (auto node: g.nodes){
    //   cout << "node " << node->nodeID << " is at time " << node->data.clock << " with value " << node->output << endl;
    // }
    //
    // return 0;
}
