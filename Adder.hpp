#ifndef Adder_hpp
#define Adder_hpp

#include <iostream>
#include "Graph.hpp"
#include "GNode.hpp"
#include "SimObj.hpp"
#include "Event.hpp"
#include <fstream>
#include <bitset>
#include <vector>
#include <queue>
#include <climits>

class Adder{

public:
  // A, B and C0
  vector<queue<Event*>*> queue_collection1;
  vector<string> queue1_name;
  // C1, C2 ... Ck
  vector<queue<Event*>*> queue_collection2;
  vector<string> queue2_name;
  // S0, S1, .. Sk-1
  vector<queue<Event*>*> queue_collection3;
  vector<string> queue3_name;
  // vector storing all graphs
  vector<Graph*> graph_collection;
  vector<string> graph_name;

  Adder(int k);
  void Read(int a_input, int b_input, long receive_time);
  void finalOutput();
};

#endif
