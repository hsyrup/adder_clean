//
//  SimObj.cpp
//  6730proj2
//
//  Created by cheng on 18/3/27.
//  Copyright © 2018年 cheng. All rights reserved.
//
#include <climits>
#include <iostream>
#include "SimObj.hpp"

//void SimObj::initial(int ni, int no){
    //SimObj();
//
    //id = ++idCntr;
    //numInputs = ni;
    //numOutputs = no;
//
    //vector<long> a(ni, 0);
    //inputTimes = a;
    //vector<atomic_bool> b(ni, false);
    //inputEventLock = b;
//
    //Event e;
    //queue<Event> k;
    //vector<queue<Event>> c(numInputs, k);
    //inputEvents = c;
    //vector<Event> d;
    //readyEvents = d;
//
	//numLockedInput.exchange(0);
	//isRunning = false;
	//isOnWl = false;
//}

SimObj::SimObj() {
    inputTimes.reserve(4);
    inputEvents.reserve(4);
}

SimObj::SimObj(int ni, int no): inputEvents(4), inputTimes(4){
}

//SimObj::SimObj(const SimObj& that) :
//id(that.id),
//numInputs(that.numInputs),
//numOutputs(that.numOutputs),
//inputEvents(that.inputEvents),
//inputTimes(that.inputTimes),
//clock(that.clock),
//readyEvents(that.readyEvents)
//{}

//void SimObj::initialize(int ni, int no){}

void SimObj::reset(int numInputs, int numOutputs){
	this->inputTimes.clear();
	inputEvents.clear();
    inputEventLock.clear();

    queue<Event> empty;
	readyEvents.swap(empty);

	this->numLockedInput.exchange(0);
	this->isRunning = false;
	this->isOnWl = false;

}


void SimObj::computeClock(){
	long min = LONG_MAX;
  int updateFlag = 1;
    for (int i = 0; i < numInputs; ++i) {
      if(!inputEvents[i].empty()) {
        this->inputTimes[i] = inputEvents[i].front().recvTime;
      }
      if(this->inputTimes[i] == LONG_MAX) {
        cout << "TIME NOT EXIST ERROR" << endl;
        updateFlag = 0;
        break;
      }
      //cout << "inputTimes" << this->inputTimes[i] << endl;
      if (min > this->inputTimes[i]) {
        min = this->inputTimes[i];
      }
    }
    if(updateFlag){
      this->clock = min;
    }
}

bool SimObj::isActive(){
	return active;
}

void SimObj::updateActive(){
//	bool isWaiting = false; // is Waiting on an input event
//    // i.e. pq of the input is empty and the time of last evetn is not INFINITY
//    bool allEmpty = true;
//    for (int i = 0; i < numInputs; ++i) {
//      if (inputEvents[i].empty()) {
//        if (inputTimes[i] < LONG_MAX) {
//          isWaiting = true;
//        }
//      } else {
//        allEmpty = false;
//      }
//    }
//    active = !allEmpty && !isWaiting;
    active = false;
    for(int i = 0; i < numInputs; i++){
        if(inputEvents[i].empty() || inputEvents[i].front().type != "NULL"){
            active = true;
        }
    }
}

void SimObj::populateReadyEvents(int epi, bool sepInputLock){
	long min;
    long recvTime;
    int minIdx = 0;
    int count = 0;
    bool addEvent = true;

    while(count < epi && addEvent) {
        min = LONG_MAX;
        addEvent = false;
        for(int idx = 0; idx < numInputs; ++idx) {
            if(!inputEvents[idx].empty()) {
                recvTime = inputEvents[idx].front().getRecvTime();
                //cout << recvTime << endl;

                //System.out.println("(" + id + ") event[" + idx + "], recvTime = " +
                //   recvTime + ", clock = " + this.clock + ", min = " + min);

                //if(recvTime <= this->clock && recvTime <= min && inputEvents[idx].front().type != "NULL") {
                if(recvTime <= this->clock && recvTime <= min) {
                //if(recvTime <= min) {
                    min = recvTime;
                    minIdx = idx;
                    addEvent = true;
                }
            }
        }

        if(addEvent) {
            //this->clock = min;
            this->readyEvents.push(inputEvents[minIdx].front());
            inputEvents[minIdx].pop();
            if(!inputEvents[minIdx].empty()){
              inputTimes[minIdx] = inputEvents[minIdx].front().recvTime;
            }
            ++count;
        }
    }

    if(sepInputLock)
      this->releaseAllInputLock();
}

bool SimObj::onwlFlag(){
	return this->isOnWl;
}

bool SimObj::tryLockAllInput(){
    bool ret = true;
    int i, failIdx = 0;

    for(i = 0; i < numInputs; ++i) {
        ret = inputEventLock[i].exchange(true);
        if(ret) {
            failIdx = i;
            break;
        }
    }

    if(!ret) {
        for(i = 0; i < failIdx; ++i)
            inputEventLock[i].exchange(false);
    }else
		numLockedInput.exchange(numInputs);

    return ret;
}

void SimObj::releaseAllInputLock(){
	for(int i = 0; i < numInputs; ++i){
        inputEventLock[i].exchange(false);
	}

	numLockedInput.exchange(0);
}

bool SimObj::hasLockedInput(){
	return (numLockedInput.load() > 0);
}

int SimObj::getId(){
	return id;
}

int SimObj::getIdcntr(){
	return idCntr;
}
