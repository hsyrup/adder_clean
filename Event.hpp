//
//  Event.hpp
//  6730proj2
//
//  Created by cheng on 18/3/27.
//  Copyright © 2018年 cheng. All rights reserved.
//

#ifndef Event_hpp
#define Event_hpp

#include <string>
#include <stdio.h>
#include <atomic>

using namespace std;

class Event{

public:
    Event(string tp, int v);
	string type;

    int whichIn;
	long recvTime;
	long sendTime;
    int val;
    long getRecvTime();
};

#endif /* Event_hpp */
