//
//  GNode.cpp
//  6730proj2
//
//  Created by cheng on 18/3/27.
//  Copyright © 2018年 cheng. All rights reserved.
//

#include <climits>
#include <atomic>
#include "GNode.hpp"
using namespace std;

//TODO finish the default constructor
GNode::GNode(): data(8,8), input(8){
//    for(int i = 0; i < 4; i++){
//        input.push_back(false);
//    }
}

//TODO finish the copy constructor
//GNode::GNode(const GNode& another):
//type(another.type),
//nodeID(another.nodeID),
//graphID(another.graphID),
//data(another.data),
//inNeigh(another.inNeigh),
//outNeigh(another.outNeigh),
//input(8)

//casLock(another.casLock.load()),
//input(another.input)
//output(another.output.load())
//{}

//GNode::GNode() {}

GNode::GNode(int ID) : nodeID(ID), input(8){
}

GNode::GNode(int ID, int gID) : nodeID(ID), graphID(gID), input(8){
}

GNode::GNode(SimObj d, int ID) : nodeID(ID), input(8){
}

SimObj* GNode::getData(){
	return &data;
}

void GNode::reset(){
	lockList.clear();
	casLock.exchange(false);
	data.reset(0, 0);
}

int GNode::getNodeID() { return nodeID;}

// bool hasInNeighbor(GNode n);
// bool hasOutNeighbor(GNode n);
// bool addInNeighbor(GNode n);
// bool addOutNeighbor(GNode n);

vector<GNode*> GNode::getOutNeigh(){
	return outNeigh;
}

//my modification
void GNode::setType(string t){
	type = t;
}

//cut from SimObj
int GNode::getInputIndex(int id){

	for(int i = 0; i < inNeigh.size(); i++){
		if(inNeigh[i]->nodeID == id){
			return i;
		}
	}
	return -1;
}

int GNode::simulate(GNode& myNode, int epi, bool sepInputLock){
		myNode.data.computeClock();// update the clock, can do at the end if null msgs are propagated initially
		//cout << "node "<< myNode.nodeID << " is at time: "<< myNode.data.clock << endl;
    myNode.data.populateReadyEvents(epi, sepInputLock); // fill up readyEvents,
		//cout << "node "<< myNode.nodeID << " is at time: "<< myNode.data.clock << endl;
    //myNode.data.updateActive();
		//cout << "x1" <<endl;
    int retVal = this->data.readyEvents.size();
		//cout << "x2" <<endl;
    while (!this->data.readyEvents.empty()) {
    	Event e = this->data.readyEvents.front();
    	this->data.readyEvents.pop();

    	// if(e.type == "NULL"){
      //       cout << e.type << " at node " << myNode.nodeID << " with recvTime " << e.recvTime << endl;
    	// }
			//cout << "y" <<endl;
      //assert(e.getRecvObj() == myNode && myNode.getData() == this->data); // should already own a lock
      execEvent(myNode, e);
    }

    return retVal;
}

Event GNode::makeEvent(GNode& recvObj, string label, int msg, long sendTime, long delay){
	Event e(label, msg);
	if(delay <= 0){
		delay = this->MIN_DELAY;
	}
	long recvTime;
	if(sendTime == LONG_MAX){
		recvTime = LONG_MAX;
	}
	else {
		recvTime = sendTime + delay;
	}

	e.recvTime = recvTime;
	e.sendTime = sendTime;
	e.type = label;
	e.val = msg;

    return e;
}

void GNode::execEvent(GNode& sendNode, Event& e){
	//cout << "execEvent" << endl;
	int ans = INT_MAX;
	string t;
	//cout << "tt" << endl;
	// if (sendNode.inNeigh.size() > e.whichIn){
	// 	cout << "msg from " << sendNode.inNeigh[e.whichIn]->nodeID << " to " << sendNode.nodeID << ", type: " << e.type
	// 	<< " value: " << e.val << " recv: " << e.recvTime << endl;
	// }
	//cout << "t1" << endl;
  if(e.type == "NULL"){
		cout << "null" << endl;
		for(auto &itr: outNeigh){
				Event ne = makeEvent(*itr, "NULL", output.load(), e.getRecvTime(), MIN_DELAY);
				int in = itr->getInputIndex(nodeID);
				itr->recvEvent(in, ne);
		}
	}else{
		//cout << "r" << endl;
		t = "REGULAR";
		//cout << "e.whichIn "<< e.whichIn << endl;
		input[e.whichIn].exchange(e.val);
		//cout << "r0.1" << endl;
		if(this->type == "AND"){
			ans = 0;
    	for(auto &&itr : input){ans += (int)itr.load();}
    	if(ans == data.numInputs) output.exchange(1);
    	else output.exchange(0);
		}
  	else if(this->type == "OR"){
      	ans = 0;
      	for(auto &&itr : input){ans += itr.load();}
      	if(ans >= 1) output.exchange(1);
      	else output.exchange(0);
  	}
  	else if(this->type == "NOT"){
      	ans = input[0].load();
      	if(ans == 1) output.exchange(0);
      	else output.exchange(1);
  	}
    else if(this->type == "PORT"){
				//cout << "rp" << endl;
        ans = input[0].load();
        output.exchange(ans);
    }
		//cout << "r1" << endl;
    // cout << "excutEvent: node[" << nodeID << "] output:" << output << endl;
    // cout << "inputs:" ;
    // for(int i = 0; i < data.numInputs; i++){cout << input[i].load() <<",";}
    // cout << endl;

    //makeEvent and fanout
    for(auto &itr: outNeigh){
        Event ne = makeEvent(*itr, t, output.load(), e.getRecvTime(), MIN_DELAY);
				//cout << "e.recvTime() " << e.recvTime << endl;
        int in = itr->getInputIndex(nodeID);
        itr->recvEvent(in, ne);
				// if (itr->nodeID == 14){
				// 	cout << "+++++++++++\n"
				// 		<< "++++++++++\n"
				// 		<< "node: " << nodeID << " sends node 14 a msg, then it is at time " << data.clock<< "\n"
				// 		<< "recvTime" << ne.recvTime << "\n"
				// 		<< "+++++++++++\n"
				// 		<< "++++++++++"
				// 		<< endl;
				//}
    }
  }
}

void GNode::recvEvent(int in, Event& e){
	if(!(in >= 0 && in < data.inputEvents.size() && e.recvTime >= this->data.inputTimes[in])){
    //if(e.type == "NULL"){return;}
		//cout << data.inputTimes.size() << endl;
    if(in == -1) cout << "ERROR: GNode not found"<<endl;
		cout << "ERROR: "<< "inputTimes = " << this->data.inputTimes[in] << endl;
	}
  else{
    e.whichIn = in;
		// if(this->data.inputEvents[in].empty()){
		// 	if (this->data.inputTimes[in] < e.getRecvTime()) {
	  //     this->data.inputTimes[in] = e.getRecvTime();
	  //   }
		// }
		if(e.type != "NULL"){
			if(!this->data.inputEvents[in].empty()){
				while(this->data.inputEvents[in].front().type == "NULL"){
					this->data.inputEvents[in].pop();
				}
			}
		}
    this->data.inputEvents[in].push(e);
		this->data.inputTimes[in] = this->data.inputEvents[in].front().recvTime;
	// 	cout << "Node["<< nodeID << "]recv msg at " << e.recvTime << "," << "in:" << in << " with type: " << e.type << endl;
	// 	cout << "Node["<< nodeID << "]channel " << in << " has inputTimes " << this->data.inputTimes[in] << endl;
  }
}


// int GNode::mapOutNeighbors(int inNodeId){
// 	int idx;
// 	for(int i = 0; i < outNeigh.size(); i++){
// 		if(outNeigh.nodeID == inNodeId){
// 			return i;
// 		}
// 	}
// 	return -1;
// }

//bool GNode::acuireLock(){
//	return !casLock.exchange(true);
//}
//
//void GNode::releaseLock(){
//	casLock.exchange(false);
//}


//bool GNode::tryLockInput(GNode& from){
//    int inIdx = from.getNodeID();
//
//	if(!data.inputEventLock[inIdx].exchange(true)){
//        data.numLockedInput++;
//		return true;
//	}
//	return false;
//}
//
//void GNode::releaseInputLock(GNode& from){
//	int inIdx = from.getNodeID();
//
//	data.inputEventLock[inIdx].exchange(false);
//	data.numLockedInput--;
//}
